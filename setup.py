from cx_Freeze import setup, Executable

base = None

executables = [Executable("src/main.py", base=base)]

packages = ["idna"]
options = {
    'build_exe': {
        'packages': packages,
    },
}

setup(
    name="game of life",
    options=options,
    version="0.1",
    description='',
    executables=executables
)
