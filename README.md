# Projekt programistyczny - Rozszerzona gra w życie

## Możliwości
* Zatrzymywanie i wznawianie symulacji
* Zmiana szybkości symulacji oraz obliczanie okreslonej liczby kroków z pominięciem animacji
* Zmiana reguł automatu według zasad opisanych w [artykule na Wikipedii](ttps://pl.wikipedia.org/wiki/Gra_w_%C5%BCycie#Modyfikacje_gry_w_%C5%BCycie) 
* Zmiana liczby stanów automatu - przy liczbie większej niż 2 w momencie kiedy liczba żywych 
sąsiaów komórki wskazuje na ożywienie komórki stan jest zwiększany o 1
* Wczytywanie plików tekstowych ze stanem początkowym symulacji według formatu


    ...
    X Y S
    ...
Gdzie X, Y to współrzędne komórki, a S to numer stanu - domyślnie 0 - nieżywa komórka     
* Zmiana rozmiaru planszy w pliku konfiguracyjnym pythona conf.py 

## Użycie
* Plik main.exe w katalogu 'build' uruchamia program
* wczytanie pliku z konfiguracją planszy jest możliwe poprzez wpisanie jego nazwy w polu 'Path' i zatwierdzenie przyciskiem 'set board'. Podobnie inne parametry.
* Ponowne skompilowanie prijektu pythona do plików natywnych jest mozliwe poprzez komendę 'python setup.py build' w katalogu 'python'


