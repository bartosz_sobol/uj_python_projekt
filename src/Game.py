import pygame

from src.Cell import Cell
from src.conf import *


class Game:
    """
    Game class
    """

    def __init__(self, screen):
        """
        simple init
        :param screen: pygame screen
        """
        self.screen = screen
        self.cells = []
        self.live = None
        self.born = None
        self.step_time = 100
        self.run = False
        self.steps = int(DEFAULT_STEPS)
        self.max_state = int(DEFAULT_STATES) - 1

        self.set_rule(DEFAULT_RULE_1, DEFAULT_RULE_2)

        l_font = pygame.font.SysFont("monospace", 20)
        l_0 = l_font.render("0", 1, (255, 255, 255))
        l_1 = l_font.render("1", 1, (255, 255, 255))
        l_2 = l_font.render("2", 1, (255, 255, 255))
        l_3 = l_font.render("3", 1, (255, 255, 255))
        l_4 = l_font.render("4", 1, (255, 255, 255))
        l_5 = l_font.render("5", 1, (255, 255, 255))
        l_6 = l_font.render("6", 1, (255, 255, 255))
        l_7 = l_font.render("7", 1, (255, 255, 255))
        l_8 = l_font.render("8", 1, (255, 255, 255))
        l_9 = l_font.render("9", 1, (255, 255, 255))
        pygame.draw.rect(screen, (200, 200, 200), [WIDTH * CELL_SIZE, 0, 223, 495])
        pygame.draw.rect(screen, (255, 255, 255), [WIDTH * CELL_SIZE + 10, 430, 20, 20])
        pygame.draw.rect(screen, (0, 0, 0), [WIDTH * CELL_SIZE + 40, 430, 20, 20])
        pygame.draw.rect(screen, (180, 180, 180), [WIDTH * CELL_SIZE + 70, 430, 20, 20])
        pygame.draw.rect(screen, (0, 0, 180), [WIDTH * CELL_SIZE + 100, 430, 20, 20])
        pygame.draw.rect(screen, (0, 180, 180), [WIDTH * CELL_SIZE + 130, 430, 20, 20])
        pygame.draw.rect(screen, (180, 180, 0), [WIDTH * CELL_SIZE + 10, 460, 20, 20])
        pygame.draw.rect(screen, (180, 0, 180), [WIDTH * CELL_SIZE + 40, 460, 20, 20])
        pygame.draw.rect(screen, (180, 0, 0), [WIDTH * CELL_SIZE + 70, 460, 20, 20])
        pygame.draw.rect(screen, (0, 180, 0), [WIDTH * CELL_SIZE + 100, 460, 20, 20])
        pygame.draw.rect(screen, (255, 0, 0), [WIDTH * CELL_SIZE + 130, 460, 20, 20])
        screen.blit(l_0, (WIDTH * CELL_SIZE + 15, 430))
        screen.blit(l_1, (WIDTH * CELL_SIZE + 45, 430))
        screen.blit(l_2, (WIDTH * CELL_SIZE + 75, 430))
        screen.blit(l_3, (WIDTH * CELL_SIZE + 105, 430))
        screen.blit(l_4, (WIDTH * CELL_SIZE + 135, 430))
        screen.blit(l_5, (WIDTH * CELL_SIZE + 15, 460))
        screen.blit(l_6, (WIDTH * CELL_SIZE + 45, 460))
        screen.blit(l_7, (WIDTH * CELL_SIZE + 75, 460))
        screen.blit(l_8, (WIDTH * CELL_SIZE + 105, 460))
        screen.blit(l_9, (WIDTH * CELL_SIZE + 135, 460))

    def read_file(self, path):
        """
        Reads inout file and updates board
        :param path: file path
        """
        self.cells = []
        cell_map = [[0 for _ in range(WIDTH)] for _ in range(HEIGHT)]

        with open(path, "r") as file:
            for line in file:
                xy = list(map(int, line.split()))
                cell_map[xy[0]][xy[1]] = 1

        for (i, row) in enumerate(cell_map):
            self.cells.append([])
            for (j, state) in enumerate(row):
                self.cells[i].append(Cell(state, i, j))
        self.render()

    def render(self):
        """
        Renders board on pygame screen
        """
        for row in self.cells:
            for cell in row:
                cell.render(self.screen)

    def check_neighboors(self):
        """
        perform check_neighbourd on all cells
        """
        for row in self.cells:
            for cell in row:
                cell.check_neighbours(self.cells)

    def breed(self):
        """
        Performs state update on all cells
        """
        for row in self.cells:
            for cell in row:
                cell.breed(self.max_state, self.live, self.born)

    def speed_up(self):
        """
        Increses simulation speed
        """
        if self.step_time:
            self.step_time -= 10

    def slow_down(self):
        """
        decreses simulation speed
        """
        self.step_time += 10

    def set_rule(self, live, born):
        self.live = list(map(lambda l: int(l), live))
        self.born = list(map(lambda l: int(l), born))

    def set_states_number(self, number_of_states):
        self.max_state = number_of_states - 1

    def calculate_steps(self, steps):
        """
        Calculates given number of steps and renders result
        :param steps: number of steps
        """
        self.steps = steps
        while self.steps:
            self.check_neighboors()
            self.breed()
            self.steps -= 1

    def wait_time(self):
        """
        return time intil next step
        """
        if not self.steps:
            return self.step_time
        else:
            self.steps -= 1
            return 0
