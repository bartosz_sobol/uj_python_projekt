import pygame

from src.conf import WIDTH, CELL_SIZE, HEIGHT


class Cell:
    """
    Cell class
    """

    def __init__(self, state, x, y):
        """
        :param state: initial cell state
        :param x: x position
        :param y: y position
        """
        self.state = state
        self.x = x
        self.y = y
        self.neighbours = 0

    def check_neighbours(self, cells):
        """
        Counts live cells in neighbourhood
        :param cells: simulation board
        """
        self.neighbours = 0
        self.neighbours += cells[self.x - 1][self.y].state is not 0
        self.neighbours += cells[(self.x + 1) % WIDTH][self.y].state is not 0
        self.neighbours += cells[self.x - 1][self.y - 1].state is not 0
        self.neighbours += cells[self.x][self.y - 1].state is not 0
        self.neighbours += cells[(self.x + 1) % WIDTH][self.y - 1].state is not 0
        self.neighbours += cells[self.x - 1][(self.y + 1) % HEIGHT].state is not 0
        self.neighbours += cells[self.x][(self.y + 1) % HEIGHT].state is not 0
        self.neighbours += cells[(self.x + 1) % WIDTH][(self.y + 1) % HEIGHT].state is not 0

    def breed(self, max_state, live=(2, 3), born=tuple([3])):
        """
        Changes cells state accordingly to rule
        :param max_state: max state
        :param live: rule
        :param born: rule
        """
        if self.neighbours in born and self.state < max_state:
            self.state += 1
        elif self.neighbours not in live and self.state > 0:
            self.state -= 1

    def render(self, screen):
        """
        renders cell on pygame screen
        :param screen: screen
        """

        color = (255, 255, 255)
        if self.state == 1:
            color = (0, 0, 0)
        elif self.state == 2:
            color = (180, 180, 180)
        elif self.state == 3:
            color = (0, 0, 180)
        elif self.state == 4:
            color = (0, 180, 180)
        elif self.state == 5:
            color = (180, 180, 0)
        elif self.state == 6:
            color = (180, 0, 180)
        elif self.state == 7:
            color = (180, 0, 0)
        elif self.state == 8:
            color = (0, 180, 0)
        elif self.state == 9:
            color = (255, 0, 0)

        pygame.draw.rect(screen, color, [self.x * CELL_SIZE, self.y * CELL_SIZE, CELL_SIZE, CELL_SIZE])
