import sys

import pygame

from src.GUI import GUI
from src.Game import Game
from src.conf import *

pygame.init()

screen = pygame.display.set_mode((WIDTH * CELL_SIZE + 225, HEIGHT * CELL_SIZE))

game = Game(screen)
game.read_file(DEFAULT_INPUT)
gui = GUI(screen, game)


def main():
    while True:
        for event in pygame.event.get():
            gui.react(event)
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
        if not game.steps:
            game.render()
        if game.run:
            game.check_neighboors()
            game.breed()
        pygame.display.flip()
        pygame.time.wait(game.wait_time())


if __name__ == "__main__":
    main()
