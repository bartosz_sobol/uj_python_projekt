import thorpy

from src.conf import *


class GUI:
    """
    GUI class
    """

    def __init__(self, screen, game):
        """
        creates gui elements
        :param screen: pygame screen
        :param game: Game class object
        """
        self.game = game
        self.screen = screen
        self.start_btn = thorpy.make_button("Start", func=self.start)
        self.stop_btn = thorpy.make_button("Stop", func=self.stop)

        self.speed_title = thorpy.make_text("Speed")
        self.speed_plus_btn = thorpy.make_button("+", func=self.increase_speed)
        self.speed_minus_btn = thorpy.make_button("-", func=self.decrease_speed)

        self.set_rule_btn = thorpy.make_button("Set rule", func=self.set_rule)

        self.set_states_btn = thorpy.make_button("Set states", func=self.set_states_number)

        self.calculate_steps_btn = thorpy.make_button("Calculate steps", func=self.instant_steps)
        self.set_board_btn = thorpy.make_button("Set board", func=self.set_board)

        big_buttons = [self.start_btn,
                       self.stop_btn,
                       self.set_rule_btn,
                       self.calculate_steps_btn,
                       self.set_board_btn]

        self.rule_insert_1 = thorpy.Inserter.make(name="Rule: ", value=DEFAULT_RULE_1)
        self.rule_insert_2 = thorpy.Inserter.make(name="       / ", value=DEFAULT_RULE_2)
        self.states_insert = thorpy.Inserter.make(name="States: ", value=DEFAULT_STATES)
        self.steps_insert = thorpy.Inserter.make(name="Steps", value=DEFAULT_STEPS)
        self.path_insert = thorpy.Inserter.make(name="Path: ", value=DEFAULT_INPUT, )

        for button in big_buttons:
            button.set_size((140, 28))
        self.speed_minus_btn.set_size((70, 28))
        self.speed_plus_btn.set_size((70, 28))
        self.rule_insert_1.set_size((30, 20))
        self.rule_insert_2.set_size((30, 20))
        self.path_insert.set_size((170, 20))

        self.box = thorpy.Box.make(elements=[self.start_btn,
                                             self.stop_btn,
                                             self.speed_title,
                                             self.speed_minus_btn,
                                             self.speed_plus_btn,
                                             self.steps_insert,
                                             self.calculate_steps_btn,
                                             self.rule_insert_1,
                                             self.rule_insert_2,
                                             self.set_rule_btn,
                                             self.states_insert,
                                             self.set_states_btn,
                                             self.path_insert,
                                             self.set_board_btn])
        self.menu = thorpy.Menu(self.box)
        for element in self.menu.get_population():
            element.surface = self.screen
        self.box.set_topleft((WIDTH * CELL_SIZE, 0))
        self.box.blit()
        self.box.update()
        self.set_rule()

    def react(self, event):
        """
        react on event
        :param event: evemt
        """
        self.menu.react(event)

    def set_rule(self):
        """
        Handles click calling Game class method
        """
        self.game.set_rule(self.rule_insert_1.get_value(), self.rule_insert_2.get_value())

    def set_states_number(self):
        """
        Sets number of cell states
        """
        print(self.states_insert.get_value())
        self.game.set_states_number(int(self.states_insert.get_value()))

    def decrease_speed(self):
        """
        Handles click calling Game class method
        """
        self.game.slow_down()

    def increase_speed(self):
        """
        Handles click calling Game class method
        """
        self.game.speed_up()

    def start(self):
        """
        Handles click calling Game class method
        """
        self.game.run = True

    def stop(self):
        """
        Handles click calling Game class method
        """
        self.game.run = False

    def instant_steps(self):
        """
        Handles click calling Game class method
        """
        self.game.calculate_steps(int(self.steps_insert.get_value()))

    def set_board(self):
        """
        Handles click calling Game class method
        """
        self.game.read_file(self.path_insert.get_value())
