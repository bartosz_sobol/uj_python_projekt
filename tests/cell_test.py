import unittest

from src.Cell import Cell


class CellTestCase(unittest.TestCase):
    def setUp(self):
        self.cell = Cell(1, 2, 3)

    def test_init(self):
        self.assertEqual(self.cell.state, 1)
        self.assertEqual(self.cell.x, 2)
        self.assertEqual(self.cell.y, 3)

    def test_check_nbs(self):
        cells = [[Cell(1, 0, 0), Cell(1, 0, 0), Cell(1, 0, 0), Cell(1, 0, 0), Cell(1, 0, 0)],
                 [Cell(1, 0, 0), Cell(0, 0, 0), Cell(1, 0, 0), Cell(0, 0, 0), Cell(1, 0, 0)],
                 [Cell(1, 0, 0), Cell(0, 0, 0), Cell(1, 0, 0), Cell(0, 0, 0), Cell(1, 0, 0)],
                 [Cell(1, 0, 0), Cell(0, 0, 0), Cell(1, 0, 0), Cell(0, 0, 0), Cell(1, 0, 0)],
                 [Cell(1, 0, 0), Cell(0, 0, 0), Cell(1, 0, 0), Cell(0, 0, 0), Cell(1, 0, 0)]]
        self.cell.check_neighbours(cells)
        self.assertEqual(self.cell.neighbours, 6)

    def test_breed(self):
        self.cell.breed()
        self.assertEqual(self.cell.state, 0)
