import unittest

import pygame

from src.Game import Game
from src.conf import *


class GameTestCase(unittest.TestCase):
    def setUp(self):
        screen = pygame.display.set_mode((WIDTH * CELL_SIZE + 200, HEIGHT * CELL_SIZE))
        self.game = Game(screen)

    def test_read(self):
        self.game.read_file("../samples/glider_gun.txt")
        self.assertEqual(self.game.cells[0][0].state, 0)
        self.assertEqual(self.game.cells[1][7].state, 1)

    def test_set_rule(self):
        self.game.set_rule("234/456")
        self.assertEqual(self.game.live, [2, 3, 4])
        self.assertEqual(self.game.born, [4, 5, 6])

    def test_sped(self):
        self.game.speed_up()
        self.game.speed_up()
        self.assertEqual(self.game.step_time, 80)
        self.game.slow_down()
        self.assertEqual(self.game.step_time, 90)
