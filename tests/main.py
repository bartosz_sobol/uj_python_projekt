import unittest

from tests.cell_test import CellTestCase
from tests.game_test import GameTestCase

suite1 = unittest.TestLoader().loadTestsFromTestCase(CellTestCase)
suite2 = unittest.TestLoader().loadTestsFromTestCase(GameTestCase)
alltests = unittest.TestSuite([suite1, suite2])

if __name__ == '__main__':
    unittest.main()
